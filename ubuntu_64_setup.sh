#!/bin/bash

## THIS SCRIPT HAS NOT BEEN TESTED YET
##
## Also, it's a little hard to test in general, since you really need a fresh OS install...
##
## This script will atttempt to install some software that I commonly use in my analyses
## This has been written for a system running a 64-bit version of an Ubuntu fork (Linux Mint).
## It could theoretically also work on a Debian distribution, but I wouldn't count on it
##
## It is assumed that git is already installed (and that is was most likely used
## to download the repository housing this script)
##
## The default sudo timeout is 15 mins, so the script may hang on a password reprompt depending on 
## the speed of the internet connection.
## The sudo timeout can be changed by performing the following:
##
##   1. In the terminal, type 'sudo visudo'
##   2. At the line that says 'Defaults     env_reset', add in the desired timeout:
##         
##          for instance, change to 'Defaults   env_reset,timestamp_timeout=30'
##          to set it for 30 mins
##   3. Ctrl-X to close, and press Y to save
##   4. Remember to delete that extra bit afterwards! (if you want the default again)
####################################################################################################


## First find the version name of Ubuntu and Mint distros
ubu_ver=$(cat /etc/os-release | grep "UBUNTU_CODENAME" | cut -f2 -d"=")
mint_ver=$(cat /etc/os-release | grep "VERSION_CODENAME" | cut -f2 -d"=")

## Now go to the home directory, and set up some directories there (if they don't yet exist)
##   * Downloads folder
##   * repos for git repositories
##   * bin for binaries
##   * bin/prog_dirs for software source folders
cd $HOME
mkdir Downloads
mkdir repos
mkdir repos/software_repos
mkdir bin
mkdir bin/prog_dirs


## Now install some Ubuntu packages:
sudo apt-get install -y screen
sudo apt-get install -y default-jre
sudo apt-get install -y ncbi-blast+
sudo apt-get install -y gdebi-core
sudo apt-get install -y apt-transport-https
sudo apt-get install -y bedtools
sudo apt-get install -y vcftools
sudo apt-get install -y vim


## Add repositories ##

    ## Add repository for R - requires adding key to the Ubuntu keyring
    echo "deb http://cran.rstudio.com/bin/linux/ubuntu "$ubu_ver/ | sudo tee -a /etc/apt/sources.list
    gpg --keyserver keyserver.ubuntu.com --recv-key E084DAB9
    gpg -a --export E084DAB9 | sudo apt-key add -

    ## Sublime Text 3
    wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | sudo apt-key add -
    echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublime-text.list

    ## insync (google drive API)
    sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys ACCAF35C
    mkdir /etc/apt/sources.list.d
    echo "deb http://apt.insynchq.com/mint $mint_ver non-free contrib" | sudo tee -a /etc/apt/sources.list.d/insync.list

    ## f.lux (monitor color adjustment)
    sudo add-apt-repository -y ppa:nathan-renniewaldock/flux

## Do a manual update and upgrade of all packages (apparently --force-yes option is required)
sudo apt-get -y --force-yes update
sudo apt-get -y --force-yes upgrade

## Single apt-get install for above repositories
sudo apt-get install -y \
    r-base r-base-dev \
    sublime-text \
    insync \
    fluxgui


## Install programs lacking packages or repositories ##

    ## RStudio
    wget -P ~/Downloads/ http://www.rstudio.org/download/latest/stable/desktop/ubuntu64/rstudio-latest-amd64.deb
    sudo gdebi -n ~/Downloads/rstudio*.deb

    ## Anaconda (Python 3 version)
    wget https://repo.continuum.io/archive/Anaconda3-5.0.1-Linux-x86_64.sh -O ~/Downloads/anaconda3.sh
    bash ~/Downloads/anaconda3.sh -b -p $HOME/anaconda3
    echo "export PATH=$PATH:$HOME/anaconda3/bin" | sudo tee -a ~/.profile
    source ~/.profile

        ## Install biopython in Anaconda        
        source activate root
        pip install biopython
        source deactivate

    ## PLINK 1.9
    wget https://www.cog-genomics.org/static/bin/plink180109/plink_linux_x86_64.zip -O ~/Downloads/plink.zip
    mkdir ~/Downloads/plink_files
    unzip ~/Downloads/plink.zip -d ~/Downloads/plink_files
    mv ~/Downloads/plink_files/plink ~/bin/plink2
    mv ~/Downloads/plink_files/prettify ~/bin/prettify
    
    ## TASSEL 5 - clone directly into prog_dirs
    cd ~/bin/prog_dirs
    git clone https://bitbucket.org/tasseladmin/tassel-5-standalone.git    
    echo "" | sudo tee -a $HOME/.profile
    echo "# .jar aliases" | sudo tee -a $HOME/.profile
    echo "export alias tasseljar=/home/brian/bin/prog_dirs/tassel-5-standalone/sTASSEL.jar"

    ## LinkImpute - note this is a .jar file, so needs an alias in the $PATH
    wget http://www.cultivatingdiversity.org/uploads/5/8/2/9/58294859/linkimpute.tar.gz -O ~/Downloads/linkimp.tar.gz
    mkdir ~/bin/prog_dirs/linkimpute
    tar -zxvf ~/Downloads/linkimp.tar.gz -C ~/bin/prog_dirs/linkimpute

    echo "export alias linkimp=$HOME/bin/prog_dirs/linkimpute/LinkImpute.jar" | sudo tee -a $HOME/.profile
    
    ## BEAGLE - this is likewise a .jar file
    mkdir ~/bin/prog_dirs/beagle
    wget http://faculty.washington.edu/browning/beagle/beagle.08Jun17.d8b.jar -O ~/bin/prog_dirs/beagle/beagle.08Jun17.d8b.jar
    echo "export alias beajar=$HOME/bin/prog_dirs/beagle/beagle.08Jun17.d8b.jar" | sudo tee -a $HOME/.profile


## Clean up temp files in Downloads directory
cd ~/Downloads
rm *.deb *.sh *.zip *tar.gz

echo "Time to restart..."

exit 0;