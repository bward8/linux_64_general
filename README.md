# README

### What is this repository for?

This repository houses a handful of simple bash scripts and code snippets to
aid in working on 64-bit Linux systems. I will admit that it is
unapologetically aimed towards my own preferences. It assumes that the user is
using a 64-bit Ubuntu-based distribution (I typically use Linux Mint Xfce).
In addition to a script that aids in setting up a fresh Linux install to my
liking, this repository also houses some text files with helpful one-liners.

## Setup Script

The script that is included here for system setup will perform actions:

* Create directory ~/bin (if it doesn't already exist)
* Install several Linux programs (if not included with distro): screen, etc.
* Install the following:
	- PLINK 1.9 (PLINK 2.0 when it is released to beta)
	- VCFTools
	- BEDTools
	- BEAGLE
	- NCBI BLAST
	- R + RStudio
	- Anaconda
	- Sublime 3 Text Editor
	- Insync (Google Drive API)
	- Flux GUI (screen color adjust based on time of day)

## Who do I talk to? 

Brian Ward - brian.phillip.ward@gmail.com

